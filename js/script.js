function clone(obj) {
    const copy = {};
    for (let key in obj) {
        copy[key] = obj[key];
                if(typeof obj[key] === "object" && obj[key] != null){
                 clone(obj[key]);
                }
            }
    return copy;
};


const obj = {
  name: "Andrew",
  age: 18,
  nations:{
    first: 'Austria',
    second: 'Germany',
    third: 'Ukraine'
  }
};

alert(clone(obj));
